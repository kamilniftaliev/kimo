import React from 'react';
import { configure, addDecorator, addParameters } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withKnobs } from '@storybook/addon-knobs';
import { themes } from '@storybook/theming';
import { withTests } from '@storybook/addon-jest';
import { withA11y } from '@storybook/addon-a11y';
import StoryRouter from 'storybook-react-router';
import styled from 'styled-components';

const StorybookGlobalStyles = styled.main`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100vh;
  width: 100vw;
`

import testsResult from '../.jest/tests-result.json';
import { CSS } from '../src/components/ui';

const req = require.context('../src', true, /.story.tsx$/);

function loadStories() {
  req.keys().forEach(req);
}

addDecorator(withKnobs);
addDecorator(withA11y);
addDecorator(withInfo);

addDecorator(StoryRouter());

addParameters({
  options: {
    theme: themes.dark,
  },
  viewport: {
    defaultViewport: 'iphone5',
  },
});

addDecorator(
  withTests({ results: testsResult, filesExt: '(\\.test\\.tsx?)$' }),
);

addDecorator(story => (
  <StorybookGlobalStyles>
    <CSS.global />
    {story()}
  </StorybookGlobalStyles>
));

configure(loadStories, module);
