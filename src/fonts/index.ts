import { css } from 'styled-components';

import RobotoLightttf from './Roboto/Roboto-Light.ttf';
import RobotoLightwoff from './Roboto/Roboto-Light.woff';
import RobotoLightwoff2 from './Roboto/Roboto-Light.woff2';

import RobotoMediumttf from './Roboto/Roboto-Regular.ttf';
import RobotoMediumwoff from './Roboto/Roboto-Regular.woff';
import RobotoMediumwoff2 from './Roboto/Roboto-Regular.woff2';

const fonts = css`
  @font-face {
    font-family: Roboto;
    font-style: normal;
    font-weight: 300;
    font-display: swap;
    src: url('${RobotoLightwoff2}') format('woff2'), /* Modern Browsers */
        url('${RobotoLightwoff}') format('woff'), /* Modern Browsers */
        url('${RobotoLightttf}') format('truetype');
  }

  @font-face {
    font-family: Roboto;
    font-style: normal;
    font-weight: 500;
    font-display: swap;
    src: url('${RobotoMediumwoff2}') format('woff2'), /* Modern Browsers */
        url('${RobotoMediumwoff}') format('woff'), /* Modern Browsers */
        url('${RobotoMediumttf}') format('truetype');
  }
`;

export default fonts;
