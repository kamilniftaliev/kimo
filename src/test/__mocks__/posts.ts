import { GET_POSTS } from '../../queries/posts';

export const mocks = [
  {
    request: {
      query: GET_POSTS,
      variables: { cityId: 1, dateId: -1 },
    },
    result: {
      data: {
        cities: [
          {
            id: 1,
            title: 'Bakı',
          },
          {
            id: 2,
            title: 'Xaçmaz',
          },
        ],
        posts: [
          {
            city: 'Bakı',
            date: 'Dünən',
            id: 5,
            text: 'TEX4 TEXT4 TEXT4 - BAKI',
            views: 131,
          },
          {
            city: 'Bakı',
            date: 'Dünən',
            id: 4,
            text: 'TEX3 TEXT3 TEXT3 - BAKI',
            views: 131,
          },
          {
            city: 'Bakı',
            date: 'Dünən',
            id: 1,
            text: 'TEX TEXT TEXT - BAKI',
            views: 2131,
          },
          {
            city: 'Bakı',
            date: '13 Sentyabr',
            id: 3,
            text: 'TEX2 TEXT2 TEXT2 - BAKI',
            views: 63463,
          },
        ],
      },
    },
  },
];

export default mocks;
