import initStoryshots from '@storybook/addon-storyshots';
import { imageSnapshot } from '@storybook/addon-storyshots-puppeteer';
import devices from 'puppeteer/DeviceDescriptors';
// import path from 'path';

async function customizePage(page): Promise<string[]> {
  const models = ['iPhone 5', 'iPhone X'];
  return page.emulate(devices[models[0]]);
  // return page.emulate(devices[models[0]]);
  // return Promise.all<string>(
  //   models.map(async (phone): Promise<string> => page.emulate(devices[phone])),
  // );
}

// const getScreenshotOptions = ({
//   context: { kind, story },
// }): Record<string, any> => {
//   return {
//     path: `./src/screenshots/${kind}-${story}.png`,
//   };
// };

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const getMatchOptions = (): Record<string, any> => {
  return {
    failureThreshold: 0.01,
    failureThresholdType: 'percent',
  };
};

export const saveResults = (name: string): void => {
  const storybookUrl = `file:///${process.env.PWD}/public/storybook`;

  initStoryshots({
    suite: '',
    storyNameRegex: name,
    test: imageSnapshot({
      // getScreenshotOptions,
      getMatchOptions,
      storybookUrl,
      customizePage,
    }),
  });
};

export default saveResults;
