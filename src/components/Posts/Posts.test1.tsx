import React from 'react';
import { act } from 'react-dom/test-utils';
import { mount } from 'enzyme';
import { MockedProvider } from '@apollo/react-testing';
// import wait from 'waait';

// jest.mock('@sentry/browser');
import { Posts, SelectorsContainer, Item } from './Posts';
import { saveResults } from '../../test/functions';
import { mocks } from '../../test/__mocks__/posts';

describe('Posts', () => {
  it('renders with mock data', async () => {
    let wrapper;

    await act(async () => {
      wrapper = mount(
        <MockedProvider mocks={mocks} removeTypename>
          <Posts />
        </MockedProvider>,
      );

      // await wait(3000);
      await new Promise(resolve => setTimeout(resolve));
      wrapper.update();

      expect(wrapper.find(SelectorsContainer)).toBeTruthy();
      expect(wrapper.find(Item)).toBeTruthy();
      expect(wrapper).toMatchSnapshot();
    });
  });
});

saveResults('Posts');
