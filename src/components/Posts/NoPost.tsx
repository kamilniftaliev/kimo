import React from 'react';
import styled from 'styled-components';

import { translate } from '../../utils';
import { Text as ItemText } from './Item';
import { CSS, Selector } from '../ui';

// import UnamusedIcon from '../../images/emojies/unamused.png';
// import UnamusedIcon from '../../images/emojies/facepalm-1.gif';
import UnamusedIcon from '../../images/emojies/facepalm-2.gif';
// import UnamusedIcon from '../../images/emojies/facepalm-3.gif';
// import UnamusedIcon from '../../images/emojies/facepalm-4.webp';

const Text = styled(ItemText)`
  font-size: 22px;
  word-break: initial;
  max-width: 500px;
  margin-left: auto;
  margin-right: auto;
`;

const Unamused = styled.img.attrs(() => ({
  alt: 'Heç nə tapılmadı',
  src: UnamusedIcon,
}))`
  display: block;
  margin-top: 20px;
  width: 100%;
  ${CSS.centered()}
`;

export interface Props {
  city: string;
  date: Selector.List.ItemProps;
}

const getMonth = ({ id, label }: Selector.List.ItemProps): string => {
  const date = label.toLowerCase();

  if (id < 2) return date;
  const [month, day] = date.split(' ');

  return `${translate(`${month}Num`)} ${day}`;
};

const NoPost: React.FC<Props> = ({ city, date }: Props): React.ReactElement => {
  return (
    <>
      <Unamused />
      <Text>
        Görünür
        <strong>{` ${city} `}</strong>
        şəhərində
        <strong>{` ${getMonth(date)} `}</strong>
        heç kim heç kimi itirib axtarmayıb.
      </Text>
    </>
  );
};

const MemoNoPost = React.memo(NoPost);

export { MemoNoPost as NoPost };

export default MemoNoPost;
