import React from 'react';
import { storiesOf } from '@storybook/react';
import { MockedProvider } from '@apollo/react-testing';

import { Posts } from './Posts';
import { mocks } from '../../test/__mocks__/posts';

storiesOf('Components', module)
  .addParameters({ jest: ['Posts'] })
  .add('Posts', () => (
    <MockedProvider mocks={mocks} addTypename={false}>
      <Posts />
    </MockedProvider>
  ));
