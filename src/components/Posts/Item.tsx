import React from 'react';
import styled from 'styled-components';

import { CSS } from '../ui';

const Container = styled.article`
  ${CSS.containerBox}
  margin-top: 20px;
`;

const Header = styled.header`
  display: flex;
  justify-content: space-around;
  font-weight: 500;
`;

export const Text = styled.p`
  color: #424242;
  text-align: center;
  margin-bottom: 0;
  word-break: break-all;
`;

// const Stats = styled(Header)``;

// const Icon = styled.img.attrs(() => ({
//   alt: 'icon',
// }))`
//   display: inline-block;
//   margin-right: 5px;

//   width: 18px;
// `;

// const IconContainer = styled.span`
//   display: flex;
// `;

export interface Props {
  id: number;
  text: string;
  views: number;
  city: string;
  date: string;
}

const Item: React.FC<Props> = ({
  text,
  city,
  date,
}: Props): React.ReactElement => {
  return (
    <Container>
      <Header>
        <span>{city}</span>
        <span>{date}</span>
      </Header>
      <Text>{text}</Text>
      {/* <Stats as="footer">
        <IconContainer>
          <Icon src={ViewsIcon} /> {views}
        </IconContainer>
        <IconContainer>
          <Icon src={LocationIcon} /> {date}
        </IconContainer>
      </Stats> */}
    </Container>
  );
};

const MemoItem = React.memo(Item);

export { MemoItem as Item };

export default MemoItem;
