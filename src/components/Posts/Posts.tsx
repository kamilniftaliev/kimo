import React, { useState } from 'react';
import { useQuery } from '@apollo/react-hooks';
import styled from 'styled-components';

import { dateOptions, translate } from '../../utils';
import { CSS, Selector, Preloader } from '../ui';
import { GET_POSTS } from '../../queries/posts';

import { Item } from './Item';
import { NoPost } from './NoPost';

export const SelectorsContainer = styled.header`
  display: flex;
  justify-content: space-between;
  ${CSS.centered()}
`;

const PostsContainer = styled.section`
  ${CSS.centered()}
`;

const SearchSelector = styled(Selector.Selector)`
  flex-basis: calc(50% - 10px);
`;

const Posts: React.FC = (): JSX.Element => {
  const [city, setCity] = useState<Selector.List.ItemProps>(null);
  const [date, setDate] = useState<Selector.List.ItemProps>(dateOptions[0]);

  const {
    loading,
    data: { posts, cities },
  } = useQuery(GET_POSTS, {
    variables: {
      cityId: city?.id,
      dateId: date.id,
    },
  });

  const hasCitiesAndNotSelectedAny = cities && !city;

  if (loading || hasCitiesAndNotSelectedAny) {
    if (hasCitiesAndNotSelectedAny) setCity(cities[0]);
    return <Preloader />;
  }

  return (
    <>
      <SelectorsContainer>
        <SearchSelector
          textProp="title"
          onSelect={setCity}
          sortAlphabetically
          value={city}
          options={cities}
          title={translate('cities')}
        />
        <SearchSelector
          isListCentered
          onSelect={setDate}
          value={date}
          options={dateOptions}
          title={translate('date')}
        />
      </SelectorsContainer>
      <PostsContainer>
        {posts.length ? (
          // eslint-disable-next-line react/prop-types
          posts.map(props => <Item key={props.id} {...props} />)
        ) : (
          <NoPost city={city.title} date={date} />
        )}
      </PostsContainer>
    </>
  );
};

export { Item };

const MemoPosts = React.memo(Posts);

// export { MemoPosts as Posts };

// export default MemoPosts;
export { Posts };

export default Posts;
