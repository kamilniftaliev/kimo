import React from 'react';
import { mount } from 'enzyme';

import { dateOptions } from '../../../utils';
import { Selector, Select, List } from './Selector';
import { saveResults } from '../../../test/functions';

let mockOnSelect;
let mockOnClose;
let wrapper;

describe('Selector', () => {
  beforeEach(() => {
    mockOnSelect = jest.fn();
    mockOnClose = jest.fn();

    wrapper = mount(
      <Selector
        options={dateOptions}
        value={dateOptions[1]}
        title="Tarix"
        onSelect={mockOnSelect}
        onClose={mockOnClose}
      />,
    );
  });

  afterEach(() => {
    mockOnSelect = null;
    mockOnClose = null;
    wrapper.unmount();
    wrapper = null;
  });

  it('renders by default', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('renders list on click', () => {
    // Click on selector label
    wrapper.find(Select).simulate('click');

    // List rendered
    expect(wrapper.find(List.List).exists()).toBeTruthy();
    expect(wrapper).toMatchSnapshot();
  });

  it('closes the list on item select', () => {
    wrapper.find(Select).simulate('click');

    // Simulate click on first item
    wrapper
      .find(List.Item)
      .first()
      .simulate('click');

    expect(mockOnSelect).toBeCalled();
    // List rendered
    expect(wrapper.find(List.List).exists()).toBeFalsy();

    expect(wrapper).toMatchSnapshot();
  });

  it('closes the list on X click', () => {
    wrapper.find(Select).simulate('click');

    // List rendered
    expect(wrapper.find(List.List).exists()).toBeTruthy();

    // Click on X icon
    wrapper.find(List.CloseButton).simulate('click');

    expect(wrapper.find(List.List).exists()).toBeFalsy();

    expect(mockOnClose).toBeCalled();

    // expect(wrapper).toMatchSnapshot();
  });
});

saveResults('Selector');
