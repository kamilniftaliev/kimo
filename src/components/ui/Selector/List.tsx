import React, { Fragment } from 'react';
import styled from 'styled-components';

import CloseIcon from '../../../images/close.svg';

import { Overlay } from '../Overlay/Overlay';

const Container = styled.section`
  max-width: 300px;
  flex-grow: 1;
  position: relative;
  border-radius: 5px;

  background-color: #fff;
  box-shadow: 0 3px 10px rgba(0, 0, 0, 0.3);
`;

const Title = styled.header`
  text-align: center;
  background-color: #f7f7f7;
  font-size: 18px;
  padding-top: 10px;
  padding-bottom: 10px;
  border-bottom: 1px solid #d1cfcf;
  border-radius: 5px 5px 0 0;
`;

const OptionsContainer = styled(({ isListCentered, ...rest }) => (
  <ul {...rest} />
))`
  max-height: 77vh;
  overflow-y: auto;
  background-color: #fff;
  border-radius: 0 0 5px 5px;
  font-size: 16px;
  padding-top: 10px;
  padding-bottom: 20px;

  ${({ isListCentered }: Props): string =>
    isListCentered ? 'text-align: center;' : ''}
`;

export const Item = styled.li`
  padding: 10px 20px;
`;

const Letter = styled.li`
  background-color: #ededed;
  text-transform: uppercase;
  font-weight: bold;
  padding: 5px 20px;
`;

export const CloseButton = styled.button`
  position: absolute;
  bottom: -18px;
  left: 0;
  right: 0;
  width: 38px;
  height: 38px;
  padding: 10px;
  background-color: #fff;
  margin: auto;
  border: none;
  border-radius: 50%;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.7);
`;

const Close = styled.img`
  display: block;
  opacity: 0.6;
  width: 18px;
  height: 18px;
`;

export interface ItemProps {
  id: number;
  label?: string;
  title?: string;
}

export interface Props {
  /** The title at the of the list */
  title: string;
  /** Array of options with required "id" and "label" fields */
  options: ItemProps[];
  /** Specifies whether options should be sorted alphabetically */
  sortAlphabetically?: boolean;
  /** Callback function for an option select */
  onSelect: Function;
  /** Callback function for closing the list by clicking on X */
  onClose?: React.MouseEventHandler;
  /** The field name that will read text from */
  textProp?: string;
  /** If true list items will be centered */
  isListCentered?: boolean;
}

const getItem = (
  item: ItemProps,
  onSelect: Function,
  textProp: string,
): JSX.Element => (
  <Item onClick={(): void => onSelect(item)} key={`${item.id}`}>
    {item[textProp]}
  </Item>
);

function getSortedList(
  options: ItemProps[],
  onSelect: Function,
  textProp: string,
): JSX.Element[] {
  return options
    .sort((a, b) => {
      const prevLabel = a[textProp].toLowerCase();
      const nextLabel = b[textProp].toLowerCase();
      return prevLabel < nextLabel ? -1 : 1;
    })
    .map((item, index, arr) => {
      const showLetter =
        !index || arr[index - 1][textProp][0] !== item[textProp][0];
      const itemElem = getItem(item, onSelect, textProp);

      return showLetter ? (
        <Fragment key={`${item.id}`}>
          <Letter>{item[textProp][0]}</Letter>
          {itemElem}
        </Fragment>
      ) : (
        itemElem
      );
    });
}

const List: React.FC<Props> = ({
  title,
  options,
  sortAlphabetically,
  onSelect,
  onClose,
  textProp = 'label',
  isListCentered = false,
}: Props): JSX.Element => {
  let list: JSX.Element[];

  if (sortAlphabetically) {
    list = getSortedList(options, onSelect, textProp);
  } else {
    list = options.map(item => getItem(item, onSelect, textProp));
  }

  return (
    <Overlay>
      <Container>
        <Title>{title}</Title>
        <OptionsContainer isListCentered={isListCentered}>
          {list}
        </OptionsContainer>
        <CloseButton onClick={onClose}>
          <Close src={CloseIcon} alt="Bağla" />
        </CloseButton>
      </Container>
    </Overlay>
  );
};

const MemoList = React.memo(List);

export { MemoList as List };
