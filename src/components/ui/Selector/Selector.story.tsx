import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
// import { linkTo } from '@storybook/addon-links';

import { Selector, List } from './Selector';

const options = [
  {
    id: '1',
    label: 'Option 1',
  },
  {
    id: '2',
    label: 'Option 2',
  },
];

storiesOf('UI', module)
  .addParameters({ jest: ['Selector'] })
  .add('Selector', () => {
    return (
      <Selector
        value="Some value"
        title="Hello"
        onSelect={action('Selected option:')}
        onClose={action('Closed list by clicking X')}
        options={options}
      />
    );
  })
  .add('List', () => {
    return (
      <List
        title="Hello"
        onSelect={action('Selected option:')}
        onClose={action('Closed list by clicking X')}
        options={options}
      />
    );
  });
