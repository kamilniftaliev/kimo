import React, { useState } from 'react';
import styled from 'styled-components';

// import {
//   List,
//   Props as ListProps,
//   ItemProps as ListItemProps,
//   // ItemProps,
//   Item,
//   CloseButton,
// } from './List';

import * as List from './List';

import * as CSS from '../styles';

export { List };

const Container = styled.div`
  ${CSS.containerBox}
  padding: 10px;
`;

export const Select = styled.span`
  display: block;
  position: relative;
  font-size: 16px;
`;

const Label = styled.span`
  display: block;
  text-align: center;
  width: calc(100% - 20px);
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
  font-weight: 500;
`;

const Triangle = styled.span`
  display: block;
  position: absolute;
  top: 7px;
  right: 5px;

  ${CSS.triangle('#828282', 6, 'bottom')}
`;

interface Props extends List.Props {
  /** The text on selector */
  value: List.ItemProps;
  /** For custom styling with styled-components */
  className?: string;
}

const Selector: React.FC<Props> = ({
  value,
  options,
  title,
  onClose,
  onSelect,
  sortAlphabetically,
  className,
  textProp = 'label',
  isListCentered = false,
}: Props): JSX.Element => {
  const [listOpened, setListOpened] = useState<boolean>(false);

  return (
    <Container className={className}>
      <Select onClick={(): void => setListOpened(true)}>
        <Label>{value[textProp]}</Label>
        <Triangle />
      </Select>
      {listOpened && (
        <List.List
          isListCentered={isListCentered}
          textProp={textProp}
          onClose={(e): void => {
            setListOpened(false);
            if (onClose) onClose(e);
          }}
          onSelect={(item: List.ItemProps): void => {
            onSelect(item);
            setListOpened(false);
          }}
          sortAlphabetically={sortAlphabetically}
          title={title}
          options={options}
        />
      )}
    </Container>
  );
};

const MemoSelector = React.memo(Selector);

export { MemoSelector as Selector };

export default MemoSelector;
