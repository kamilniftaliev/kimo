import React, { FC } from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import PostsIcon from '../../../images/posts.svg';
import WriteIcon from '../../../images/write.svg';
import SettingsIcon from '../../../images/settings.svg';
import InfoIcon from '../../../images/info.svg';
import UserIcon from '../../../images/user.svg';

export const Container = styled.div`
  display: flex;
  justify-content: space-around;
  position: fixed;
  width: 100%;
  height: 45px;
  bottom: 0;
  left: 0;

  background-color: #fff;
  border-top: 1px solid #ededed;
  box-shadow: 0 -2px 5px rgba(0, 0, 0, 0.2);
`;

export const Item = styled(Link)`
  padding: 10px;
`;

export const Icon = styled.img.attrs(() => ({
  alt: 'Menu Icon',
}))`
  height: 25px;
  width: 20px;
`;

const Menu: FC = (): JSX.Element => {
  return (
    <Container>
      <Item to="/">
        <Icon src={PostsIcon} />
      </Item>
      <Item to="post">
        <Icon src={UserIcon} />
      </Item>
      <Item to="yaz">
        <Icon src={WriteIcon} />
      </Item>
      <Item to="ayarlar">
        <Icon src={SettingsIcon} />
      </Item>
      <Item to="kimo">
        <Icon src={InfoIcon} />
      </Item>
    </Container>
  );
};

const MemoMenu = React.memo(Menu);

export { MemoMenu as Menu };

export default MemoMenu;
