import React from 'react';
import { storiesOf } from '@storybook/react';

import { Menu } from './Menu';

storiesOf('UI', module)
  .addParameters({ jest: ['Menu'] })
  .add('Menu', () => <Menu />);
