import React from 'react';
import { mount } from 'enzyme';
import { StaticRouter } from 'react-router-dom';

import { Menu, Container, Item, Icon } from './Menu';
import { saveResults } from '../../../test/functions';

describe('Menu', () => {
  it('renders correctly', () => {
    const wrapper = mount(
      <StaticRouter>
        <Menu />
      </StaticRouter>,
    );
    expect(wrapper.find(Container).exists()).toBeTruthy();
    expect(wrapper.find(Item).exists()).toBeTruthy();
    expect(wrapper.find(Icon).exists()).toBeTruthy();
    expect(wrapper).toMatchSnapshot();
  });
});

saveResults('Menu');
