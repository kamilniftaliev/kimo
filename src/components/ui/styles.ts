import {
  createGlobalStyle,
  css,
  FlattenSimpleInterpolation,
} from 'styled-components';

export const global = createGlobalStyle`
  body,
  input,
  button {
    font-family: Roboto, Arial, sans-serif;
    font-weight: 300;
    box-sizing: border-box;
  }
  
  body {
    margin: 0;
    background-color: #f7f7f7;
    font-size: 14px;
  }

  ul {
    margin: 0;
    padding: 0;
    list-style: none;
  }

  button:focus,
  input:focus {
    outline: 0;
  }
`;

export const containerBox = css`
  box-sizing: border-box;
  max-width: 500px;
  min-width: 110px;
  padding: 15px;
  font-size: 16px;
  border-radius: 2px;
  background-color: #fff;
  box-shadow: 0 2px 3px rgba(0, 0, 0, 0.2);
`;

export const triangle = (
  color: string,
  size: number,
  dir: string,
): FlattenSimpleInterpolation =>
  css`
    ${dir === 'top' &&
      `border-color: transparent transparent ${color} transparent`};
    ${dir === 'bottom' &&
      `border-color: ${color} transparent transparent transparent`};
    border-style: solid;
    border-width: ${size}px ${size}px ${size}px ${size}px;
    height: 0px;
    width: 0px;
  `;

export const centered = (maxWidth = 500): FlattenSimpleInterpolation =>
  css`
    margin-left: auto;
    margin-right: auto;
    max-width: ${maxWidth}px;
  `;
