import React, {
  useRef,
  useImperativeHandle,
  forwardRef,
  ForwardRefExoticComponent,
  MutableRefObject,
} from 'react';

export interface InputProps {
  hasError?: boolean;
  value?: string;
  onChange?: Function;
  type?: string;
  placeholder?: string;
  ref?: MutableRefObject<{}>;
  readOnly?: boolean;
}

export const ForwardedInput = (): ForwardRefExoticComponent<InputProps> =>
  /* eslint-disable react-hooks/rules-of-hooks */
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  forwardRef(({ hasError, ...rest }: InputProps, ref) => {
    const inputRef = useRef();
    useImperativeHandle(ref, () => ({
      focus: (): void => inputRef.current.focus(),
    }));
    return <input {...rest} ref={inputRef} />;
  });

export default ForwardedInput;
