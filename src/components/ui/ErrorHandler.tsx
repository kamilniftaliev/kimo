import React, { Component } from 'react';
import { withScope, init, captureException } from '@sentry/browser';
import styled from 'styled-components';

import ErrorEmoji from '../../images/error.svg';

init({
  dsn: 'https://607d90cd8a014998a90418f1f9f1326c@sentry.io/1319950',
  release: process.env.CI_COMMIT_SHA,
});

const Container = styled.div`
  display: flex;
  box-sizing: border-box;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  position: fixed;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  padding-left: 20px;
  padding-right: 20px;

  text-align: center;
  background-color: #fff;
`;

const Text = styled.p`
  font-size: 20px;
  margin-bottom: 0;
`;

const SmallText = styled.p`
  position: absolute;
  bottom: 0;
`;

export const ErrorImage = styled.img.attrs(() => ({
  alt: 'Error Emoji',
  src: ErrorEmoji,
}))`
  width: 50vw;
  height: 50vw;
  opacity: 0.7;
`;

const Link = styled.a`
  color: blue;
  border-bottom: 1px solid blue;
  text-decoration: none;
`;

interface InitState {
  hasError?: boolean;
  eventId?: string;
}

class ErrorHandler extends Component<React.ReactElement> {
  state: InitState = {};

  componentDidCatch(error, errorInfo): void {
    withScope(scope => {
      scope.setExtras(errorInfo);
      const eventId = captureException(error).slice(0, 10);
      this.setState({ eventId, hasError: true });
    });
  }

  render(): React.ReactNode {
    const { hasError, eventId } = this.state;
    const { children } = this.props;

    if (hasError) {
      return (
        <Container>
          <ErrorImage />
          <Text>Bu xəta üzərində artıq işləyirik</Text>
          <Text>Müvəqqəti narahatçılığa görə üzr istəyirik.</Text>
          <Text>
            <Link href="/">KİMO.AZ</Link>
          </Text>
          <SmallText>{`Xəta #${eventId}`}</SmallText>
        </Container>
      );
    }

    return children;
  }
}

const MemoErrorHandler = React.memo(ErrorHandler);

export { MemoErrorHandler as ErrorHandler };

export default MemoErrorHandler;
