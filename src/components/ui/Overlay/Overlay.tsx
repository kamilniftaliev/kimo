import React, { FC } from 'react';
import styled, { createGlobalStyle } from 'styled-components';

import { Container as MenuContainer } from '../Menu/Menu';

const GlobalStyles = createGlobalStyle`
  html, body {
    overflow: hidden;
  }  

  ${MenuContainer} {
    display: none;
  }
`;

export const OverlayElement = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  box-sizing: border-box;
  position: fixed;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  padding: 5%;
  z-index: 1;

  background-color: rgba(0, 0, 0, 0.75);
`;

interface Props {
  /** Children at the center */
  children: JSX.Element;
}

const Overlay: FC<Props> = ({ children }: Props): JSX.Element => {
  return (
    <OverlayElement>
      {children}
      <GlobalStyles />
    </OverlayElement>
  );
};

const MemoOverlay = React.memo(Overlay);

export { MemoOverlay as Overlay };

export default MemoOverlay;
