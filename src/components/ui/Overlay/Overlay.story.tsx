import React from 'react';
import { storiesOf } from '@storybook/react';

import { Overlay } from './Overlay';

storiesOf('UI', module)
  .addParameters({ jest: ['Overlay'] })
  .add('Overlay', () => <Overlay>AA</Overlay>);
