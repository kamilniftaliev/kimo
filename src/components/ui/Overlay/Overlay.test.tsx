import React from 'react';
import { mount } from 'enzyme';

import { Overlay, OverlayElement } from './Overlay';
import { saveResults } from '../../../test/functions';

describe('Overlay background', () => {
  it('renders by default', () => {
    const wrapper = mount(
      <Overlay>
        <span>some text</span>
      </Overlay>,
    );
    expect(wrapper.text()).toEqual('some text');
    expect(wrapper.find(OverlayElement).exists()).toBeTruthy();
    expect(wrapper).toMatchSnapshot();
  });
});

saveResults('Overlay');
