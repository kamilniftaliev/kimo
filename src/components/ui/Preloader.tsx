import React from 'react';
import styled from 'styled-components';

import LogoIcon from '../../images/kimo.svg';

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  position: fixed;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
`;

const Logo = styled.img.attrs(() => ({
  src: LogoIcon,
  alt: 'KİMO',
}))``;

const Preloader: React.FC = (): JSX.Element => {
  return (
    <Container>
      <Logo />
    </Container>
  );
};

const MemoPreloader = React.memo(Preloader);

export { MemoPreloader as Preloader };

export default MemoPreloader;
