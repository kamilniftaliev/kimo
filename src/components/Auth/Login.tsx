import React, { useState, useRef } from 'react';
import styled from 'styled-components';

import UnamusedEmoji from '../../images/emojies/unamused.png';

import { translate } from '../../utils';
import { Custom, CSS } from '../ui';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const Container = styled(({ step, ...rest }) => <section {...rest} />)`
  ${CSS.containerBox}
  left: 0;
  right: 0;
  margin-left: auto;
  margin-right: auto;
  margin-top: ${({ step = 1 }): string => (step === 1 ? '13vh' : '8vh')};
  padding-top: 30px;
  padding-bottom: 30px;
  text-align: center;
`;

const Title = styled.h3`
  font-weight: 300;
  margin-top: 0;
`;

const Input = styled(Custom.ForwardedInput())`
  display: block;
  width: 120px;
  padding: 10px;
  margin-left: auto;
  margin-right: auto;

  text-align: center;
  font-size: 18px;
  border: 1px solid rgba(0, 0, 0, 0.2);
  border-radius: 2px;

  &:focus {
    border-color: #4879d7;
  }

  ${({ hasError }: Custom.InputProps): string =>
    hasError ? 'border-color: #f60122;' : ''}
`;

const PasswordContainer = styled.div`
  position: relative;
  width: 120px;
  margin: 15px auto;
`;

const InvisibleNumberInput = styled(Custom.ForwardedInput())`
  opacity: 0;
  position: absolute;
  height: 100%;
  top: 0;
  left: 0;

  &:focus + ${Input} {
    border-color: #4879d7;
  }
`;

const SubmitButton = styled.button`
  width: 120px;
  padding-top: 10px;
  padding-bottom: 10px;
  border-radius: 3px;
  font-size: 16px;
  background-color: #3884ff;
  color: #fff;
  border: 2px solid #357bed;
  margin-top: 10px;
`;

const ErrorText = styled.p`
  position: relative;
  background-color: #f60122;
  border-radius: 3px;
  color: #fff;
  margin-top: 10px;
  margin-bottom: 0;
  padding: 6px;

  &:before {
    display: block;
    position: absolute;
    top: -14px;
    right: 0;
    left: 0;
    margin-left: auto;
    margin-right: auto;
    content: '';
    ${CSS.triangle('#f60122', 7, 'top')}
  }
`;

const Emoji = styled.img.attrs(() => ({
  alt: 'emoji',
  src: UnamusedEmoji,
}))`
  width: 20px;
  vertical-align: middle;
  margin-left: 5px;
  margin-top: -2px;
`;

const getValidInput = ({
  target: { value },
}: React.ChangeEvent<Custom.InputProps>): string =>
  value.replace(/\D/g, '').slice(0, 4);

const errors = [
  '',
  <ErrorText>
    {translate('postError1')}
    <Emoji />
  </ErrorText>,
  <ErrorText>
    {translate('postError2')}
    <Emoji />
  </ErrorText>,
  <ErrorText>
    {translate('postError3')}
    <Emoji />
  </ErrorText>,
  <ErrorText>
    {translate('postError4')}
    <Emoji />
  </ErrorText>,
];

const Login: React.FC = (): JSX.Element => {
  const [postId, setPostId] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [step, setStep] = useState<number>(1);
  const [errorCode, setErrorCode] = useState<number>(0);

  const postIdInput = useRef(null);
  const passwordInput = useRef(null);

  const getErrorCode = (): number => {
    // Empty post id input
    if (!postId.length) return 1;
    // Less symbols than supposed to be
    if (postId.length < 4) return 2;

    if (step >= 2) {
      // Empty password input
      if (!password.length) return 3;
      // Less symbols than supposed to be
      if (password.length < 4) return 4;
    }

    return 0;
  };

  const signIn = (): void => {
    const newErrorCode = getErrorCode();

    setErrorCode(newErrorCode);

    // If there is an error
    if (newErrorCode) {
      if (step === 1) return postIdInput.current.focus();
      if (step === 2) {
        // If post id has an error
        if (newErrorCode < 3) {
          postIdInput.current.focus();
          return setStep(1);
        }

        setTimeout(() => passwordInput.current.focus(), 500);
      }
    } else if (step === 1) {
      // If it's the first step and no errors found move to next step
      setStep(2);
      setTimeout(() => passwordInput.current.focus(), 500);
    }
  };

  const onPostIdChange = (e: React.ChangeEvent<Custom.InputProps>): void =>
    setPostId(getValidInput(e));

  const onPasswordChange = (e: React.ChangeEvent<Custom.InputProps>): void =>
    setPassword(getValidInput(e));

  return (
    <Container step={step}>
      <Title>{translate('loginTitle')}</Title>
      <Input
        ref={postIdInput}
        hasError={errorCode && errorCode < 3}
        value={postId}
        type="number"
        placeholder={translate('postNum')}
        onChange={onPostIdChange}
      />
      {step === 2 && (
        <PasswordContainer>
          <InvisibleNumberInput
            ref={passwordInput}
            value={password}
            type="number"
            onChange={onPasswordChange}
          />
          <Input
            readOnly
            hasError={errorCode > 2}
            value={password}
            type="password"
            placeholder={translate('password')}
          />
        </PasswordContainer>
      )}
      {errors[errorCode]}
      <SubmitButton onClick={signIn}>
        {step === 1 ? translate('next') : translate('signIn')}
      </SubmitButton>
    </Container>
  );
};

const MemoLogin = React.memo(Login);

export { MemoLogin as Login };

export default MemoLogin;
