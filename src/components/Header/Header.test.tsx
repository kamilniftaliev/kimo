import React from 'react';
import { mount } from 'enzyme';
import { StaticRouter } from 'react-router-dom';

import { Header, Container } from './Header';
import { saveResults } from '../../test/functions';

describe('Header', () => {
  it('renders by default', () => {
    const wrapper = mount(
      <StaticRouter>
        <Header />
      </StaticRouter>,
    );
    expect(wrapper.find(Container).exists()).toBeTruthy();
    expect(wrapper).toMatchSnapshot();
  });
});

saveResults('Header');
