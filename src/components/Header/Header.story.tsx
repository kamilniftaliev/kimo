import React from 'react';
import { storiesOf } from '@storybook/react';

import { Header } from './Header';

storiesOf('Components', module)
  .addParameters({ jest: ['Header'] })
  .add('Header', () => <Header />);
