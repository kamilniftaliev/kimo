import React, { FC } from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

// import LogoIcon from '../images/kimo.svg';

export const Container = styled.header`
  text-align: center;
  background-color: #fff;
  border-bottom: 1px solid #ededed;
  padding: 10px;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.1);
`;

const Header: FC = (): JSX.Element => {
  return (
    <Container>
      <Link to="/">KİMO.AZ</Link>
    </Container>
  );
};

const MemoHeader = React.memo(Header);

export { MemoHeader as Header };

export default MemoHeader;
