import React from 'react';
import { mount } from 'enzyme';

import { Footer, Container } from './Footer';
import { saveResults } from '../../test/functions';

describe('Footer', () => {
  it('renders by default', () => {
    const wrapper = mount(<Footer />);
    expect(wrapper.find(Container).exists()).toBeTruthy();
    expect(wrapper).toMatchSnapshot();
  });
});

saveResults('Footer');
