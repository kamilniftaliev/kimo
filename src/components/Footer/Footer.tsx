import React, { FC } from 'react';
import styled from 'styled-components';

export const Container = styled.footer`
  text-align: center;
  margin-bottom: 70px;
`;

const Footer: FC = (): JSX.Element => {
  return (
    <Container>
      <p>© 2020 - 2021 KİMO.AZ</p>
    </Container>
  );
};

const MemoFooter = React.memo(Footer);

export { MemoFooter as Footer };

export default MemoFooter;
