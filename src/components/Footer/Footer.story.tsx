import React from 'react';
import { storiesOf } from '@storybook/react';

import { Footer } from './Footer';

storiesOf('Components', module)
  .addParameters({ jest: ['Footer'] })
  .add('Footer', () => <Footer />);
