import React, { FC, lazy, Suspense } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';
import styled from 'styled-components';

import { GRAPH_URL } from '../constants';

import { Menu, CSS, Preloader, ErrorHandler } from './ui';
import { Header } from './Header/Header';
import { Footer } from './Footer/Footer';

const Posts = lazy(() =>
  import(/* webpackChunkName: "posts" */ './Posts/Posts'),
);
const Post = lazy(() => import(/* webpackChunkName: "post" */ './Auth/Login'));

const client = new ApolloClient({
  uri: GRAPH_URL,
});

export const Container = styled.main`
  padding: 15px;
  min-height: calc(100vh - 170px);
`;

const App: FC = (): JSX.Element => {
  return (
    <ApolloProvider client={client}>
      <Router>
        <Suspense fallback={<Preloader />}>
          <ErrorHandler>
            <Header />
            <Container>
              <Switch>
                <Route path="/" exact component={Posts} />
                <Route path="/post" component={Post} />
              </Switch>
            </Container>
            <Footer />
            <Menu />
          </ErrorHandler>
        </Suspense>
      </Router>
      <CSS.global />
    </ApolloProvider>
  );
};

const MemoApp = React.memo(App);

export { MemoApp as App };

export default MemoApp;
