module.exports = {
  setupFiles: [
    './src/test/setupEnzyme.ts',
    '<rootDir>/.jest/register-context.ts',
  ],
  setupFilesAfterEnv: ['./src/test/setupTests.ts'],
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json'],
  snapshotSerializers: ['enzyme-to-json/serializer'],
  modulePaths: ['<rootDir>/src/'],
  moduleNameMapper: {
    '\\.(svg|css|gif)': '<rootDir>/src/test/__mocks__/files.ts',
  },
  testEnvironment: 'jsdom',
};
